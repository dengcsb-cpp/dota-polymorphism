#pragma once
#include "Skill.h"
#include <iostream>

class Unit;

class MeatHook :
	public Skill
{
public:
	MeatHook(Unit* actor);

	void activate(Unit* target) override;
};

