#pragma once
#include <string>
#include <vector>
using namespace std;

class Skill;

class Unit
{
public:
	Unit(string name);
	~Unit();

	string getName();
	vector<Skill*> getSkills();

	// Some properties were removed for brevity

private:
	string mName;
	// Skills are components of the Unit
	// This allows us to put 'any' skill to any Unit
	vector<Skill*> mSkills;
};

