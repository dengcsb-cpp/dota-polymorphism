#include <string>
#include <iostream>
#include "Unit.h"
#include "Avalanche.h"
#include "MeatHook.h"
#include <vector>

using namespace std;

int main()
{
	// Create Tiny
	Unit* tiny = new Unit("Tiny");
	Skill* avalanche = new Avalanche(tiny);
	tiny->getSkills().push_back(avalanche);
	
	// Create Pudge
	Unit* pudge = new Unit("Pudge");
	Skill* meatHook = new Avalanche(pudge);
	pudge->getSkills().push_back(meatHook);

	// Check if Q was pressed
	bool qPressed = true; // Assume it was pressed
	if (qPressed)
	{
		// Execute the skill at first index
		// We don't even care if this is Avalanche
		tiny->getSkills()[0]->activate(pudge);
	}

	delete tiny;
	delete pudge;

	return 0;
}