#include "Skill.h"
#include "Unit.h"

Skill::Skill(string name, Unit* actor)
{
	mName = name;
	mActor = actor;
}

string Skill::getName()
{
	return mName;
}

Unit* Skill::getActor()
{
	return mActor;
}
