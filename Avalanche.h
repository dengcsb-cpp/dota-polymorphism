#pragma once
#include "Skill.h"
#include <iostream>

class Unit;

using namespace std;

class Avalanche :
	public Skill
{
public:
	Avalanche(Unit* actor);

	void activate(Unit* target) override;
};

