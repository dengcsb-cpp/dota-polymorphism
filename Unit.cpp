#include "Unit.h"

Unit::Unit(string name)
{
	mName = name;
}

Unit::~Unit()
{
	for (int i = 0; i < mSkills.size(); i++)
	{
		delete mSkills[i];
	}
	mSkills.clear();
}

string Unit::getName()
{
	return mName;
}

vector<Skill*> Unit::getSkills()
{
	return mSkills;
}
