#include "Avalanche.h"
#include "Unit.h"

Avalanche::Avalanche(Unit* actor)
	: Skill("Avalanche", actor)
{
}

void Avalanche::activate(Unit* target)
{
	cout << getActor()->getName() << " used " << getName() << " against " << target->getName() << "!" << endl;
	// You can put complex computations here
}
