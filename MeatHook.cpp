#include "MeatHook.h"

MeatHook::MeatHook(Unit* actor)
	: Skill("Meat Hook", actor)
{
}

void MeatHook::activate(Unit* target)
{
	cout << getActor()->getName() << " used " << getName() << " against " << target->getName() << "!" << endl;
}
