#pragma once
#include <string>

class Unit;

using namespace std;

class Skill
{
public:
	Skill(string name, Unit* actor);

	string getName();
	// Actor is the one using the skill.
	// We might need access to the actor's data to perform calculation.
	Unit* getActor();

	// Skill effect will be defined by derived classes.
	virtual void activate(Unit* target) = 0;

private:
	string mName;
	Unit* mActor;
};

